package Repository_package;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtility_method.Excel_data_extractor;

public class RPut_request {

	public static String RPut_request_Tc2() throws IOException {
		ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("TestData", "Put_API", "put_TC2");
				
		String name = Data.get(1);
		String job = Data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestBody;
	}
}
