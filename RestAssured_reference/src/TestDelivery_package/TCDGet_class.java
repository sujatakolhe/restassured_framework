package TestDelivery_package;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import CommonUtility_method.Api_logs_handle;
import CommonUtility_method.Dir_Handle;
import Endpoint_Package.EGet_request;


public class TCDGet_class extends Common_method_handle_API {
	@Test

	public static void executor() throws IOException {
		File log_dir=Dir_Handle.create_DirLog("TCDGet_class");

		String get_endpoint = EGet_request .EGet_request_tc4();
		for (int i = 0; i < 5; i++) {
			int get_statuscode = get_statuscode(get_endpoint);
			System.out.println(get_statuscode);
			if (get_statuscode == 200) {
				String get_responseBody = get_responsebody(get_endpoint);
				System.out.println(get_responseBody);
				Api_logs_handle.evidence_creator(log_dir,"TCDGet_class",get_endpoint, get_responseBody);

				break;

			} else {
				System.out.println("if status code is notvalid the retry");
			}

		}

	}

	public static void get_validator(String responsBody) {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };


		JSONObject array_res = new JSONObject(responsBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		int count = dataarray.length();

		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");

			System.out.print(res_id);

			int exp_id = expected_id[i];
			String exp_first_name = expected_first_name[i];
			String exp_last_name = expected_last_name[i];
			String exp_email = expected_email[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
			Assert.assertEquals(res_email, exp_email);
		}

	}

}
