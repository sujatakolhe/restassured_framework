package TestDelivery_package;

import java.time.LocalDateTime;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import io.restassured.path.json.JsonPath;

public class TCDPost_class extends Common_method_handle_API {
	
	public static void Executor() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		String endpoint = "https://reqres.in/api/users";
		for (int i = 0; i < 5; i++) {

			int statuscode = post_statuscode(requestBody, endpoint);
			System.out.println(statuscode);
			if (statuscode == 200) {

				String responseBody = post_responsebody(requestBody, endpoint);
				System.out.println(responseBody);
				TCDPost_class.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Retry the method when status code is not found");
			}
		}

	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expected_date = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expected_date);
	}

}



