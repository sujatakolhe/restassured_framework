package Driver_package;

import java.io.IOException;

import TestDelivery_package.TestCaseDelivery_class;
import TestDelivery_package.TCDPut_class;
//import TestDelivery_package.TCDGet_class;
import TestDelivery_package.TCDPatch_class;

public class Static_Driver_class {

	public static void main(String[] args) throws IOException {
		TestCaseDelivery_class.Executor();
		TCDPut_class.Executor();
		//TCDGet_class.executor();
		TCDPatch_class.Executor();

	}

}
