package Driver_package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import CommonUtility_method.Excel_data_extractor;
public class Dynamic_Driver_class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, 
	SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<String> TC_execute = Excel_data_extractor.Excel_data_reader("TestData", "Test_Cases", "TC_name");
				
		System.out.println(TC_execute);
		int count = TC_execute.size();
		for (int i = 1; i < count; i++) {
			String TC_name = TC_execute.get(i);
			System.out.println(TC_name);
			// Call the test case class on runtime by using java.lang.reflect package
			Class<?> Test_class = Class.forName("TestDelivery_package." + TC_name);

			// Call the execute method belonging to test class captured in variable TC_name
			// by using java.lang.reflect.method class
			Method execute_method = Test_class.getDeclaredMethod("Executor");

			// Set the accessibility of method true
			execute_method.setAccessible(true);

			// Create the instance of test class captured in variable name test class name
			Object instance_of_class = Test_class.getDeclaredConstructor().newInstance();

			// Execute the test script class fetched in variable Test_class
			execute_method.invoke(instance_of_class);

		}
	}
		

	}



