package API_common_methods;

import static io.restassured.RestAssured.given;

public class Common_method_handle_API {
    
	//**** POST METHOD *****//
	public static int post_statuscode(String requestBody, String endpoint) 
	{		
		int statuscode =given().header("Content-Type", "application/json")
		      .body(requestBody)
		      .when().post(endpoint)
		      .then().extract().statusCode();
		return statuscode;		
	}
	
	public static String post_responsebody(String requestBody, String endpoint) 
	{
		
		String responseBody =given().header("Content-Type", "application/json")
		               .body(requestBody)
		               .when().post(endpoint)
		               .then().extract().response().asString();
		return responseBody;	
    }
	
	//***** PUT METHOD *****//
	
	public static int put_statuscode(String requestBody, String endpoint) 
	{		
		int statuscode =given().header("Content-Type", "application/json")
		      .body(requestBody)
		      .when().post(endpoint)
		      .then().extract().statusCode();
		return statuscode;		
	}
	
	public static String put_responsebody(String requestBody, String endpoint) 
	{
		
		String responseBody =given().header("Content-Type", "application/json")
		               .body(requestBody)
		               .when().post(endpoint)
		               .then().extract().response().asString();
		return responseBody;	
    }
	
	//***** PATCH METHOD *****//
	
	public static int patch_statuscode(String requestBody, String endpoint) 
	{		
		int statuscode =given().header("Content-Type", "application/json")
		      .body(requestBody)
		      .when().post(endpoint)
		      .then().extract().statusCode();
		return statuscode;		
	}
	
	public static String patch_responsebody(String requestBody, String endpoint) 
	{
		
		String responseBody =given().header("Content-Type", "application/json")
		               .body(requestBody)
		               .when().post(endpoint)
		               .then().extract().response().asString();
		return responseBody;	
    }
	
	//***** GET METHOD *****//
	
	public static int get_statuscode( String endpoint) 
	{		
		int statuscode =given().header("Content-Type", "application/json")
		      .when().get(endpoint)
		      .then().extract().statusCode();
		return statuscode;		
	}
	
	public static String get_responsebody( String endpoint) 
	{
		
		String responseBody =given().header("Content-Type", "application/json")
		               .when().get(endpoint)
		               .then().extract().response().asString();
		return responseBody;	
	
	}
	

}

