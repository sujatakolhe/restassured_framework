package CommonUtility_method;

import java.io.File;

public class Dir_Handle {
	public static File create_DirLog(String log_dir) {
		// fetch the current project directory
		String project_dir = System.getProperty("user.dir");
		System.out.println("the current project directory path is : " + project_dir);
		File directory = new File(project_dir + "\\API_logs\\"+log_dir);

		if (directory.exists()) {
			directory.delete();
			System.out.println(directory + ": Deleted");
			directory.mkdir();
			System.out.println(directory + ": Created");

		} else {
			directory.mkdir();
			System.out.println(directory + ": Created");
		}
		return directory;
	}

}
