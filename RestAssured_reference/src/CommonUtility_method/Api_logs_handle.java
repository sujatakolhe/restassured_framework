package CommonUtility_method;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Api_logs_handle {

	public static void evidence_creator(File dir_name, String file_name, String endpoint, String requestBody,
			String responseBody) throws IOException {
		File newfile = new File(dir_name + "\\" + file_name + ".txt");
		System.out.println("To save request and response Body we have created a new file name : " + newfile.getName());
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint Body is :" + endpoint + "\n\n");
		datawriter.write("Request Body is :" + requestBody + "\n\n");
		datawriter.write("Response Body is :" + responseBody);
		datawriter.close();

	}

	public static void evidence_creator(File dir_name, String file_name, String get_endpoint, String get_responseBody) throws IOException {
		File newfile = new File(dir_name + "\\" + file_name + ".txt");
		System.out.println("To save request and response Body we have created a new file name : " + newfile.getName());
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint Body is :" + get_endpoint + "\n\n");
		datawriter.write("Response Body is :" + get_responseBody);
		datawriter.close();
		
		
	}

}
